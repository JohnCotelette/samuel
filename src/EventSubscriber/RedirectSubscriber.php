<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

final class RedirectSubscriber implements EventSubscriberInterface
{
    private RouterInterface $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public static function getSubscribedEvents() :array
    {
        return [
            "kernel.exception" => "notFoundRedirect",
        ];
    }

    public function notFoundRedirect(ExceptionEvent $exception) :void
    {
        if ($exception->getThrowable() instanceof NotFoundHttpException) {
            $exception
                ->setResponse(new RedirectResponse(
                    $this->router->generate("home"),
                    Response::HTTP_FOUND
                ));
        }
    }
}
