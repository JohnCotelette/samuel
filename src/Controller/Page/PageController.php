<?php

namespace App\Controller\Page;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class PageController extends AbstractController
{
    #[Route("/", name: "home", methods: ["GET"])]
    public function home() :Response
    {
        return $this->render("page/home.html.twig");
    }
}
