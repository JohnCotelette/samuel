<?php

namespace App\Controller\Seo;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class SitemapController extends AbstractController
{
    private array $urls = [];

    #[Route("/sitemap.xml", name: "sitemap", defaults: ["_format" => "xml"], methods: ["GET"])]
    public function index(Request $request) :Response
    {
        $hostname = $request->getSchemeAndHttpHost();

        $this->urls[] = ["loc" => $this->generateUrl("home")];

        $response = new Response(
            $this->renderView("seo/index.xml.twig", [
                "urls" => $this->urls,
                "hostname" => $hostname
            ]),
            Response::HTTP_OK
        );

        $response->headers->set("Content-Type", "text/xml");

        return $response;
    }
}
