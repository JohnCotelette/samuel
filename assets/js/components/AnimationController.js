class AnimationController {
    constructor() {
        this.scrollable = document.getElementById("scrollable");
        this.elements = document.querySelectorAll(".noAnimated");
        this.windowHeight = window.innerHeight;
        this.limit = null;
    };

    checkPosition() {
        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            let positionFromTop = this.elements[i].getBoundingClientRect().top;

            if (positionFromTop - this.windowHeight <= this.limit) {
                element.classList.add(element.getAttribute("data-animation"));
                element.classList.remove("noAnimated");
            }
        }
    };

    init() {
        const elements = document.getElementsByClassName("noAnimated");
        const windowHeight = window.innerHeight;

        if (window.matchMedia("(max-width: 768px)").matches) {
            this.limit = -70;
        }
        else if ((window.matchMedia("(max-width: 1280px)").matches)) {
            this.limit = -100;
        }
        else if ((window.matchMedia("(max-width: 1920px)").matches)) {
            this.limit = -110;
        }
        else {
            this.limit = -130;
        }

        this.checkPosition();
    };

    initControls() {
        this.scrollable.addEventListener("scroll", this.checkPosition.bind(this));
        this.scrollable.addEventListener("resize", this.init.bind(this));
    };
}

export default AnimationController;