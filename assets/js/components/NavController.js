class NavController {
    constructor() {
        this.header = document.getElementById("mainHeader");
        this.scrollable = document.getElementById("scrollable");
        this.mobileController = document.getElementById("mobileController");
        this.jsLinks = document.getElementsByClassName("jsLinks");
    };

    initControls() {
        this.mobileController.addEventListener("click", () => {
            this.header.classList.toggle("navMobileHidden");
        }, {passive: true});

        for (let i = 0; i < this.jsLinks.length; i++) {
            this.jsLinks[i].addEventListener("click", (e) => {
                e.preventDefault();

                const y = (document
                    .getElementById(this.jsLinks[i].getAttribute("data-scroll"))
                    .getBoundingClientRect().top + this.scrollable.scrollTop);

                const shift = (window.matchMedia("(min-width: 1120px)").matches)
                    ? 145
                    : 105

                this.scrollable.scrollTo(0 , y - shift);

                if (window.matchMedia("(max-width: 1120px)").matches) {
                    if (this.jsLinks[i].nodeName === "A") {
                        this.header.classList.toggle("navMobileHidden");
                    }
                }
            });
        }
    };
}

export default NavController;