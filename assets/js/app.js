import "../css/app.scss";
import NavController from "./components/NavController";
import AnimationController from "./components/AnimationController";

const navController = new NavController();
navController.initControls();

const animationController = new AnimationController();
animationController.init();
animationController.initControls();
